<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;


/**
 * @Route("/user-crud")
 */
class UserCrudController extends AbstractController {

    /**
     * @Route("/", name="user_crud_index")
     */
    public function index(EntityManagerInterface $manager)
    {
        $users = $manager->getRepository('App:User')->findAll();
        
        return $this->render('user_crud/index.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @Route("/view/{user}", name="user_crud_view")
     */
    public function view(User $user)
    {
        return $this->render('user_crud/view.html.twig', [
            'user' => $user
        ]);
    }
    
}