<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190129110604 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE univ_ticket RENAME INDEX idx_97a0ada312469de2 TO IDX_42DC412812469DE2');
        $this->addSql('ALTER TABLE univ_ticket_user RENAME INDEX idx_bf48c371700047d2 TO IDX_44731C1C700047D2');
        $this->addSql('ALTER TABLE univ_ticket_user RENAME INDEX idx_bf48c371a76ed395 TO IDX_44731C1CA76ED395');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE univ_ticket RENAME INDEX idx_42dc412812469de2 TO IDX_97A0ADA312469DE2');
        $this->addSql('ALTER TABLE univ_ticket_user RENAME INDEX idx_44731c1c700047d2 TO IDX_BF48C371700047D2');
        $this->addSql('ALTER TABLE univ_ticket_user RENAME INDEX idx_44731c1ca76ed395 TO IDX_BF48C371A76ED395');
    }
}
